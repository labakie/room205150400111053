# Tugas 7 - Room

## Description

<p>This project main objective is to create a database inside the android application. The database itself is using SQLite with the help of room, a persistence library that allow database operation easier.</p>

## Screenshot

<table>
    <tr>
        <td style="text-align: center">Tugas 7-Room</td>
    </tr>
    <tr>
        <td>![Tugas7PapbRoom](/uploads/aa3942c81d52feac81cca29dcee12b06/Tugas7PapbRoom.gif)</td>
    </tr>
</table>


